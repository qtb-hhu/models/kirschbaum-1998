# Kirschbaum 1998 model

Implementation of model published [here](https://doi.org/10.1007/s004250050225).

## Current status

**This model is not fully implemented**

## Setting up

- `pip install pre-commit`
- `pre-commit install`
- `pip install -r code/requirements.txt`
